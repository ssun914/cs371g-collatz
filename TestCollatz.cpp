// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(10000, 500));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  10000);
    ASSERT_EQ(j, 500);
    ASSERT_EQ(v, 262);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 5));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 5);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(999000, 1000000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  999000);
    ASSERT_EQ(j, 1000000);
    ASSERT_EQ(v, 396);
}

TEST(CollatzFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval8) {
    tuple<int, int, int> t = collatz_eval(make_pair(10, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  10);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 7);
}

TEST(CollatzFixture, eval9) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 50));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 50);
    ASSERT_EQ(v, 112);
}





// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
