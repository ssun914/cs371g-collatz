# CS371g: Generic Programming Collatz Repo

* Name: Steven Sun

* EID: srs4698

* GitLab ID: ssun914

* HackerRank ID: stevensun914

* Git SHA: 640417ed3a501db584a814d68c90b5798cc6d149

* GitLab Pipelines: https://gitlab.com/ssun914/cs371g-collatz/-/pipelines

* Estimated completion time: 10

* Actual completion time: 15

* Comments: This is a Collatz submission for cs371g from last summer, and I am resubmitting it as Downing allows for this class.
